#include "weatherstation.h"



WeatherStation::WeatherStation() {
	bmeData = new Bme280();
	ws.time = 0;
	ws.bmeSensor.altitude = 0;
	ws.bmeSensor.temperature = 0;
	ws.bmeSensor.humidity = 0;
	ws.bmeSensor.pressure = 0;
	ws.rainFallSensor.rainfall = 0;
	ws.windDirectionSensor.windDirection = "North";
	ws.windDirectionSensor.windDegrees = 0;
	ws.windSpeedSensor.windSpeed = 0;
	ws.tempSensor.temperature = 0;
}

int WeatherStation::init() {
	fd = wiringPiI2CSetup(BME280_ADDRESS);
	if (fd < 0) {
		printf("Device not found");
		return -1;
	}
	return 0;
}

void WeatherStation::loop() {
	// this is where we make a loop
	while (true) {
		readBMESensorData();
		printToScreen();
		checkJsonAndStartAppending();
		createJsonPrintout();
		sleep(300);
	}
	cleanup();
}

void WeatherStation::readBMESensorData() {
	bmeData->readCalibrationData(fd, &cal);
	wiringPiI2CWriteReg8(fd, 0xf2, 0x01);   // humidity oversampling x 1
	wiringPiI2CWriteReg8(fd, 0xf4, 0x25);   // pressure and temperature oversampling x 1, mode normal
	bmeData->getRawData(fd, &raw);
	t_fine = bmeData->getTemperatureCalibration(&cal, raw.temperature);

	ws.bmeSensor.temperature = bmeData->compensateTemperature(t_fine); // C
	ws.bmeSensor.pressure = bmeData->compensatePressure(raw.pressure, &cal, t_fine) / 100; // hPa
	ws.bmeSensor.humidity = bmeData->compensateHumidity(raw.humidity, &cal, t_fine);       // %
	ws.bmeSensor.altitude = bmeData->getAltitude2(ws.bmeSensor.pressure, ws.bmeSensor.temperature);
}

void WeatherStation::printToScreen() {
	printf("{\"sensor\":\"bme280\", \"humidity\":%.2f, \"pressure\":%.2f,"
		" \"temperature\":%.2f, \"altitude\":%.2f\", \"timestamp\":%d}\n",
		ws.bmeSensor.humidity, ws.bmeSensor.pressure, ws.bmeSensor.temperature, ws.bmeSensor.altitude, (int)time(NULL));

}

void WeatherStation::checkJsonAndStartAppending() {

	std::string fileName = "webapi.json";
	try {
		jsonOut.open(fileName, std::ios::ate | std::ios::out | std::ios::in);
		std::cout << (!jsonOut.good() || jsonOut.tellp() == -1) << " tellp: " << jsonOut.tellp() << std::endl;
		isFirst = !jsonOut.good() || jsonOut.tellp() == -1;
	}
	catch (std::exception& err) {
		std::cout << err.what() << std::endl;
	}
	
	if (isFirst) {
		jsonOut.open(fileName, std::ios::app);
		std::string startOfFile = "[\n";
		jsonOut << startOfFile;
		std::cout << "startOfFile written" << std::endl;
	}
}


void WeatherStation::createJsonPrintout() {
	int timeStamp = time(NULL);


	if (!isFirst) {
		long long pos = jsonOut.tellp();
		jsonOut.seekp(pos - 3);
		std::cout << "position: " << pos << std::endl;
		std::string startOfEnd = ",\n";
		jsonOut.write(startOfEnd.c_str(), startOfEnd.length());
		std::cout << "startOfEnd written" << std::endl;
	}
	jsonOut << "	{\n";
	jsonOut << "	    \"time\": " << timeStamp << ",\n";
	jsonOut << "	    \"sensor\": [\n";
	jsonOut << "	        {\n";
	jsonOut << "	            \"type\": " << ws.bmeSensor.type << ",\n";
	jsonOut << "	            \"sensorData\": {\n";
	jsonOut << "	                \"humidity\": " << ws.bmeSensor.humidity <<",\n";
	jsonOut << "	                \"temperature\": "<< ws.bmeSensor.temperature <<",\n";
	jsonOut << "	                \"pressure\": " << ws.bmeSensor.pressure << ",\n";
	jsonOut << "	                \"altitude\": \"" << ws.bmeSensor.altitude << "m\"\n";
	jsonOut << "	            }\n";
	jsonOut << "	        },\n";
	jsonOut << "	        {\n";
	jsonOut << "	            \"type\": " << ws.windDirectionSensor.type << ",\n";
	jsonOut << "	            \"sensorData\": {\n";
	jsonOut << "	                \"windDirection\": \"" << ws.windDirectionSensor.windDirection << "\",\n";
	jsonOut << "	                \"windDirectionDegrees\": " << ws.windDirectionSensor.windDegrees << "\n";
	jsonOut << "	            }\n";
	jsonOut << "	        },\n";
	jsonOut << "	        {\n";
	jsonOut << "	            \"type\": " << ws.windSpeedSensor.type <<",\n";
	jsonOut << "	            \"sensorData\": {\n";
	jsonOut << "	                \"windspeed\": " << ws.windSpeedSensor.windSpeed << "\n";
	jsonOut << "	            }\n";
	jsonOut << "	        },\n";
	jsonOut << "	        {\n";
	jsonOut << "	            \"type\": " << ws.rainFallSensor.type << ",\n";
	jsonOut << "	            \"sensorData\": {\n";
	jsonOut << "	                \"rainfall\": " << ws.rainFallSensor.rainfall << "\n";
	jsonOut << "	            }\n";
	jsonOut << "	        },\n";
	jsonOut << "	        {\n";
	jsonOut << "	            \"type\": " << ws.tempSensor.type << ",\n";
	jsonOut << "	            \"sensorData\": {\n";
	jsonOut << "	                \"temperature\": " << ws.tempSensor.temperature << "\n";
	jsonOut << "	            }\n";
	jsonOut << "	        }\n";
	jsonOut << "	    ]\n";
	jsonOut << "	}\n";
	jsonOut << "]\n";
	jsonOut.flush();
	jsonOut.close();
}


void WeatherStation::cleanup() {
	delete bmeData;
}
