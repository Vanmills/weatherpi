#include <wiringPiI2C.h>
#include <cstdio>
#include "bme280.h"
#include <stdint.h>
#include <time.h>
#include "weatherstation.h"
#include <iostream>

int main()
{

	WeatherStation ws;
	if (ws.init() == -1) { 
		std::cout << "Unable to find the device \n\r";
		return 0;
	}
	ws.loop();
	ws.cleanup();
	
    return 0;
}