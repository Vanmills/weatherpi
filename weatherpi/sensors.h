#pragma once
#include <string>

namespace weatherStation {

	enum SensorType {
		BME280 = 0,
		WINDDIRECTION = 1,
		WINDSPEED = 2,
		RAINFALL = 3,
		TEMPERATURE = 4
	};

	class Sensor {
		SensorType sensorType;
	public:
		Sensor() : type(sensorType) {

		}

		void setSensorType(SensorType sType) {
			this->sensorType = sType;
		}

		const SensorType &type;
	};

	class BMESensor : public Sensor {
	public:
		double humidity;
		double temperature;
		double pressure;
		double altitude;
		BMESensor()
		{
			Sensor::setSensorType(SensorType::BME280);
		}
	};

	class WindDirectionSensor : public Sensor {
	public:
		double windDegrees;
		std::string windDirection;
		WindDirectionSensor()
		{
			Sensor::setSensorType(SensorType::WINDDIRECTION);
		}
	};

	class WindSpeedSensor : public Sensor {
	public:
		double windSpeed; // m/s
		WindSpeedSensor()
		{
			Sensor::setSensorType(SensorType::WINDSPEED);
		}
	};

	class RainFallSensor : public Sensor {
	public:
		double rainfall; // millimeters
		RainFallSensor()
		{
			Sensor::setSensorType(SensorType::RAINFALL);
		}
	};

	class TemperatureSensor : public Sensor {
	public:
		double temperature;
		TemperatureSensor()
		{
			Sensor::setSensorType(SensorType::TEMPERATURE);
		};
	};


	struct WeatherStationData {
		int time;
		BMESensor bmeSensor;
		WindDirectionSensor windDirectionSensor;
		WindSpeedSensor windSpeedSensor;
		RainFallSensor rainFallSensor;
		TemperatureSensor tempSensor;
	};
}