#pragma once
#include "bme280.h"
#include "sensors.h"
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>

class WeatherStation {
private:
	Bme280* bmeData;
	bme280_raw_data raw;
	bme280_calib_data cal;
	int32_t t_fine;
	int fd;
	weatherStation::WeatherStationData ws;
	std::ofstream jsonOut;
	bool isFirst;
public:
	WeatherStation();

	int init();
	void loop();
	void cleanup();
	void printToScreen();
	void checkJsonAndStartAppending();
	void createJsonPrintout();
	void readBMESensorData();
};