
typedef unsigned int uint32_t;

class GPIO{
	bool errcode; // errno if GPIO open failed
	public:
	GPIO();
	~GPIO();
	inline int get_error(); // Test for error
// Static methods
	static int pwm(int gpio,int& pwm,IO& altf);
	static void delay();
	static uint32_t peripheral_base();
	static const char *source_name(Source src);
	static const char *alt_name(IO io);
	static const char *gpio_alt_func(int gpio,IO io);
};
